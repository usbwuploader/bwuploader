# bwuploader

## Hinweis/Notice

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Download

Anwendung und Beispieldokument für Upload unter https://bitbucket.org/usbwuploader/bwuploader/downloads/


## Konfiguration

Konfigdatei unter /daten/config.xml

Die Datei kann mit einem Texteditor geöffnet und bearbeitet werden, um z.B. das Zielsystem, die Semester und Ausschüsse zu bearbeiten.

Standardmäßig ist das Q-System vorkonfiguriert. Bevor etwas auf dem P-System durchgeführt wird, empfiehlt es sich den Vorgang zunächst auf dem Q-System durchzuführen (Benutzung auf eigene Gefahr, bitte immer vorher auf einem Testsystem ausprobieren!).


## Excel-Datei bearbeiten

Im Downloadverzeichnis kann die Excel-Datei als Beispiel verwendet werden, um die Einträge zu hinterlegen, die vom Bewerbungsuploader eingetragen werden sollen.


## Anwendung & Upload starten

1. Laden Sie den Uploader herunter.

2. Starten Sie den Uploader über die ./start.bat Datei.

3. Wählen Sie die Excel-Datei mit den Einträgen aus, die Sie hochladen möchten.

4. Wählen Sie das Semester und Ihren Zulassungsausschuss aus. 

5. Geben Sie Ihre Zugangsdaten ein und Klicken Sie auf "weiter" und anschließend auf "Upload starten".



## Kontakt

Verbesserungsvorschläge und Kontakt: project-bwu@gmx.de